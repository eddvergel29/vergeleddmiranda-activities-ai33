<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\categories;
class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [

            'Sci Fi',
            'Business',
            'Fiction'
        ];
        foreach($categories as $category) {
            Categories::create(['category' => $category]);
        }
    }
}
