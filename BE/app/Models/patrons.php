<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class patrons extends Model
{
    use HasFactory;
    protected  $fillable = ['last_name', 'first_name', 'middle_name', 'email'];


    public function returned()
    {
        return $this->hasMany(returned_Books::class, 'patron_id');
    }
    
    public function borrowed()
    {
        return $this->hasMany(borrowed_Books::class, 'patron_id');
    }
}
