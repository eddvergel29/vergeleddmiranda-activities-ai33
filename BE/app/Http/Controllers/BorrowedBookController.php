<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Models\borrowed_books;
use App\Models\returned_books;
use App\Http\Requests\BorrowedBookRequest;


class BorrowedBookController extends Controller
{

    public function index()
    {
        $borrowed_book = borrowed_books::orderBy('copies', 'asc')->get();
        return response()->json([
            "message"=>"Lists of Books",
            "data" => $borrowed_book
        ]);
    }


    public function create()
    {
        
    }

    public function store(BorrowedBookRequest $request)
    {
        $borrowed_book = new borrowed_books();
        $borrowed_book->copies = $request->copies;
        $borrowed_book->book_id = $request->book_id;
        $borrowed_book->patron_id = $request->patron_id;
        $book = Books::find($borrowed_book->book_id);
        $subtracted_copies = $book->copies - $borrowed_book->copies;
        $borrowed_book->save($request->validated());
        $book->update(['copies' => $subtracted_copies]);
        return response()->json(
            ["message" => "Success",
            ["message" => "Book Borrowed",
            "data" => $borrowed_book, $book]
            ]
        );
    }

    public function show($id)
    {
        $borrowed_books= borrowed_books::find($id);
        return response()->json($borrowed_books);
    }


 

    public function update(BorrowedBookRequest $request, $id)
    {
        $borrowed_book = borrowed_books::find($id);
        $returned_book = new returned_books();
        $returned_book->copies = $request->copies;
        $returned_book->book_id = $request->book_id;
        $returned_book->patron_id = $request->patron_id;
        $book = Books::find($returned_book->book_id);
        $returned_copies = $book->copies + $returned_book->copies;
        $returned_book->save($request->validated());
        $borrowed_book->delete($request->validated());
        $book->update(['copies' => $returned_copies]);
        return response()->json(["message" => "Success",
        "data" => $borrowed_book, $book, $returned_book]);
    }

}
