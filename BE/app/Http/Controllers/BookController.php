<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Http\Requests\BookRequest;
class BookController extends Controller
{

    public function index()
    {
        $book = Books::orderBy('copies', 'asc')->get();
        return response()->json([
            "message"=>"Lists of Books",
            "data" => $book
        ]);
    }

    public function create()
    {
    
    }

    public function store(BookRequest $request)
    {
        $book = new Books();
        $book->name = $request->name;
        $book->author =$request->author;
        $book->copies =$request->copies;
        $book->category_id =$request->category_id;

        $book->save();
        return response()->json($book);
    }

    public function show($id)
    {
        $book = Books::find($id);
        return response()->json($book);
    }

    public function edit($id)
    {
        $book = Books::find($id);
        return response()->json($book);
    }

    public function update(BookRequest $request, $id)
    {
        $book = Books::find($id);
        $book->name = $request->name;
        $book->author =$request->author;
        $book->copies =$request->copies;
        $book->category_id =$request->category_id;

        $book->update();
        return response()->json($book);
    }


    public function destroy($id)
    {
        $book = Books::find($id);
        $book->delete();
        return response()->json($book);
    }
}
