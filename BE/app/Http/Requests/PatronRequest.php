<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatronRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'last_name' => 'max:50|required',
            'first_name' => 'max:50|required',
            'middle_name' => 'max:50required',
            'email' => 'required|email:rfc,dns'
        ];
    }
}
