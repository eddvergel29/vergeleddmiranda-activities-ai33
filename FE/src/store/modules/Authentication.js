import axios from "axios";
export default {
  state: {
    token: "",
    user: {},
  },
  getters: {
    authenticated(state) {
      return state.token && state.user;
    },
    user(state) {
      return state.user;
    },
  },

  actions: {
    async login({ dispatch },credential) {
      let response = await axios.post('auth/login', credential);
      return dispatch("attempt", response.data.token);
    },

    async attempt({ commit, state }, token) {
      if (token) commit("SET_TOKEN", token);
      if (!state.token) return;

      try {
        let response = await axios.get('auth/dashboard');

        commit("SET_USER", response);

       
      } catch (error) {
        commit("SET_TOKEN", null);
        commit("SET_USER", {});
      }
    },

    logout({ commit }) {
      return axios.post('auth/logout').then(() => {
        commit("SET_TOKEN", null);
        commit("SET_USER", {});
      });
    },
  },
  mutations: {
    SET_TOKEN: (state, token) => (state.token = token),
    SET_USER: (state, data) => (state.user = data),
  },
};
