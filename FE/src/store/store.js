import Vue from "vue";
import Vuex from "vuex";
import Books from "./modules/Books";
import Categories from "./modules/Category";
import Patrons from "./modules/Patrons";
import Authentication from "./modules/Authentication";
//Create store

//load Vuex
Vue.use(Vuex);

export default new Vuex.Store({
  modules: [Books, Patrons, Categories, Authentication],
});
